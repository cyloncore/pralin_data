trained with:

```bash
pralin w2v_trainer -f shakespeare.txt -o shakespeare.w2v  -g -n 10 -s 500 -l 1e-5 -i 3
```
